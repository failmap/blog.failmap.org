---
title: "Infrastucture Review February 2018"
date: 2018-02-12T13:23:36+01:00
draft: false
---

There is a lot of infra to make sure failmap runs. This is a "sneak peak" inside these systems. Of course it's not a 
"sneak peak", since everything we run is public :)

If you want to review our server config and such, better look [here](https://gitlab.com/failmap/server/),
[here](https://gitlab.com/failmap/ca) and even [here](https://gitlab.com/failmap/ci).
Please report any critical vulnerabilities, or start a mapping website and shame us over it :D


Static websites
---------------

__Failmap.org__

Running here: https://failmap.org

Static site about the project. Should get you started within a few minutes. Static HTML.
![failmap.org](/infrastructure-review-februari-2018/failmap.org.png)


__blog.failmap.org__

Running here: https://blog.failmap.org

Blog about how failmap works and major progress. Rendered with Hugo, written in Markdown.

![blog.failmap.org](/infrastructure-review-februari-2018/blog.failmap.org.png)

__internetcleanup.foundation__

Running here: https://internetcleanup.foundation

Static HTML site for the Internet Cleanup Foundation, the legal entity behind the project.

![internetcleanup.foundation](/infrastructure-review-februari-2018/internetcleanup.foundation.png)

Content sites
-------------

__faalkaart.nl__

Running here: https://faalkaart.nl

the failmap project running in the Netherlands.

![faalkaart.nl](/infrastructure-review-februari-2018/faalkaart.nl.png)

__admin.faalkaart.nl__

Running here (to view you must have the right certificate installed): https://admin.faalkaart.nl

Admin interface containing a CRUD interface with some action buttons.
It's skinned with Jango Jet, which makes it pretty slick.

![admin.faalkaart.nl](/infrastructure-review-februari-2018/admin.faalkaart.nl.png)

__grafana.faalkaart.nl__

Running here (to view you must have the right certificate installed): https://grafana.faalkaart.nl

Grafana installation to monitor running tasks. All scans we do are tasks, running "somewhere".

![grafana.faalkaart.nl](/infrastructure-review-februari-2018/grafana.faalkaart.nl.png)


3rd party services
------------------

__status.faalkaart.nl__

Running here: https://status.faalkaart.nl

Uptimerobot showing if we're down or not. Given the cache used, it should not ever be down, even if the
entire backend is on fire. Of course there can still be network failures, burning servers, proxies and whatnot,
so it still has it's use.

![status.faalkaart.nl](/infrastructure-review-februari-2018/status.faalkaart.nl.png)

__sentry__

Running here: https://sentry.io/internet-cleanup-foundation/

This helps with catching bugs / crashes on the server and visiting browsers.

![sentry.io](/infrastructure-review-februari-2018/sentry.png)


__gitlab__

Running here: https://gitlab.com/failmap

Hosts our code and CI testing. We used to do so on github, but we found gitlab to be more useful.

A github mirror still exists for promotional purposes: https://github.com/failmap/failmap

![gitlab.org](/infrastructure-review-februari-2018/gitlab.png)

__codeclimate__

Running here: https://codeclimate.com/github/failmap/failmap

Shows something about the quality of our code. Helps improve incoding, mainly pointing out to reused code now.

![codeclimate.com](/infrastructure-review-februari-2018/codeclimate.png)

__travis__

Running here: https://travis-ci.org/failmap/

Running codeclimate check and backup CI.

![travis-ci.org](/infrastructure-review-februari-2018/travis-ci.png)
