---
title: "Remote workers are here! Hyperdrive engage!"
date: 2018-03-03T10:10:51+01:00
draft: true
---
Failmap runs thousands of scans every day, and with the plans to expand to other countries, there will only be more and
more. Scaling up comes with an entirely new strategy, mainly allowing scans to be performed on other computers.

We've migrated nearly all scanners to workers: things you can run on your own machine (or "the cloud") to scan faster
and from various origins: this comes with benefits for everyone. First it makes it easier to spread out the load over
various machines instead of burst scanning from a single IP. This will mean a scan looks much more like web traffic and
that fits with our mission that our scans are not disrupting running services.

As said, scaling up also allows other peoples computers (cloud) to be used to run workers. After some searching we came
accross hyper.sh, which has a really nice infrastructure for hosting docker images. Using their services is a breeze
and allows failmap (or your implementation of failmap) to also scale up.


This is a milestone in the failmap project, and we're happy this has been reached.

The impact
----------
We started with just a few scripts that would perform all kinds of checks. They where performed one
by one, which isn't fast if you want to check thousands of domains every day.

Currenty we're using certain(celery) frameworks (redis) that (grafana) help with creating distributed
tasks.

Now, instead of scanning from one server, we're now doing this from three. And the amount will grow,
as we've been promised over a dozen machines and we didn't even started asking around. So this will
be awesome and help the project immensely.

You can find instructions on running your own scanner here:
https://failmap.readthedocs.io/en/latest/topics/running_a_remote_worker.html

What failmap scans is listed here:
https://failmap.readthedocs.io/en/latest/topics/scanners_scanning_and_ratings.html


Features
--------
- Scanners reboot on a new rollout
- It runs in bash and uses docker
- It uses celery: so tasks can be retried (on other workers) etcetera
- All free and  open



This is really a milestone in the project.

[Screenshots of task queue]


screenshot docs

